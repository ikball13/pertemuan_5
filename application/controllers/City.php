<?php
    class City extends CI_Controller{
        public function index(){
            $this->load->model("CityModel","",TRUE);
            $data['city'] = $this->CityModel->getCity();
            $this->load->view("city",$data);
        }
    }
?>